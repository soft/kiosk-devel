#!/bin/bash

# **************************************************************************************************** #
#                                                                                                      #
#                                                          :::::::::   ::::::::   ::::::::      :::    #
#   uninstall.sh                                          :+:    :+: :+:    :+: :+:    :+:   :+: :+:   #
#                                                        +:+    +:+ +:+    +:+ +:+         +:+   +:+   #
#   By: Ivan Marochkin <i.marochkin@rosalinux.ru>       +#++:++#:  +#+    +:+ +#++:++#++ +#++:++#++:   #
#                                                      +#+    +#+ +#+    +#+        +#+ +#+     +#+    #
#   Created: 2019/10/14 17:05:34 by Ivan Marochkin    #+#    #+# #+#    #+# #+#    #+# #+#     #+#     #
#   Updated: 2019/10/14 17:05:34 by Ivan Marochkin   ###    ###  ########   ########  ###     ###      #
#                                                                                                      #
# **************************************************************************************************** #

set -o xtrace

if [ $# -eq 0 ]
then
        echo -e "${C_RED}Usage ./uninstall [user_name]${C_RESET}"
        exit 1
fi

if [ -f /usr/sbin/kioskd ]
then
	echo "start uninstalling"
else
	echo "error. kioskd not exist"
	exit 1
fi

INPUT=$1
USER_HOME="/home/${INPUT}"
USER_HOME_CONF_DIR_AUTOSTART="${USER_HOME}/.config/autostart"
USER_HOME_CONF_DIR_AUTOSTART_LINK="${USER_HOME_CONF_DIR_AUTOSTART}/kioskd.desktop"
USER_PERM="${INPUT}:${INPUT}"

if [ -d $USER_HOME ]
then
	echo "User found"
else
	echo "User not found"
	exit 1
fi

rm -f /usr/sbin/kioskd
rm -f /usr/sbin/kiosk
rm -f $USER_HOME_CONF_DIR_AUTOSTART_LINK
chown -R $USER_PERM $USER_HOME_CONF_DIR_AUTOSTART
rm -f "${USER_HOME}/.kiosk.conf"
rm -f "${USER_HOME}/.kioskd.log"
rm -f "${USER_HOME}/.kiosk.log"