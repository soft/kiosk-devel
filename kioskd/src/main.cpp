/* *************************************************************************************************** */
/*                                                                                                     */
/*                                                          :::::::::   ::::::::   ::::::::      :::   */
/*  main.cpp                                               :+:    :+: :+:    :+: :+:    :+:   :+: :+:  */
/*                                                        +:+    +:+ +:+    +:+ +:+         +:+   +:+  */
/*  By: Ivan Marochkin <i.marochkin@rosalinux.ru>        +#++:++#:  +#+    +:+ +#++:++#++ +#++:++#++:  */
/*                                                      +#+    +#+ +#+    +#+        +#+ +#+     +#+   */
/*  Created: 2019/12/25 16:08:10 by Ivan Marochkin     #+#    #+# #+#    #+# #+#    #+# #+#     #+#    */
/*  Updated: 2019/12/25 16:08:10 by Ivan Marochkin    ###    ###  ########   ########  ###     ###     */
/*                                                                                                     */
/* *************************************************************************************************** */

#include "main.hpp"

/*
    KIOSKD ROSA (c) "NTC IT ROSA" 2020
    
    The daemon launches applications from the configuration file,
    starts the kiosk mode with a separate binary, monitors the state,
    ends the x-session in case of termination of applications from the configuration file
    or in case of a general failure.

    Демон запускает приложения из файла конфигурации, запускает режим киоска отдельным бинарником,
    отслеживает состояние, завершает х-сессию в случае завершения приложений из конфигурационного файла
    или при общем сбое.
*/

int main() {

    pid_t daemon = fork();
    if (!daemon) {
        Kioskd kioskd;
        kioskd.run_all_paths_from_config();
        kioskd.main_cycle();
        return (0);
    }
    return (0);
}
