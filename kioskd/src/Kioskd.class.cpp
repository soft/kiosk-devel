/* *************************************************************************************************** */
/*                                                                                                     */
/*                                                          :::::::::   ::::::::   ::::::::      :::   */
/*  Kioskd.class.cpp                                       :+:    :+: :+:    :+: :+:    :+:   :+: :+:  */
/*                                                        +:+    +:+ +:+    +:+ +:+         +:+   +:+  */
/*  By: Ivan Marochkin <i.marochkin@rosalinux.ru>        +#++:++#:  +#+    +:+ +#++:++#++ +#++:++#++:  */
/*                                                      +#+    +#+ +#+    +#+        +#+ +#+     +#+   */
/*  Created: 2020/01/22 11:28:38 by Ivan Marochkin     #+#    #+# #+#    #+# #+#    #+# #+#     #+#    */
/*  Updated: 2020/01/28 15:04:53 by Ivan Marochkin    ###    ###  ########   ########  ###     ###     */
/*                                                                                                     */
/* *************************************************************************************************** */

#include "Kioskd.class.hpp"

int Kioskd::_prepare_config_and_log_strings(void) {

    /* Prepares paths before opening files. */
    /* Подготовливает пути перед открытием файлов. */
    struct passwd *passwd;
    int user_id = getuid();
    if (!user_id) {
        std::cerr << "ERROR: Can't detect user id.\n";
        return (1);
    }
    /* Request to PASSWD for user information. */
    /* Запрос информации о пользователе. */
    passwd = getpwuid(user_id);
    if (!passwd) {
        std::cerr << "ERROR: Can't detect user name from getpwuid.\n";
        return (1);
    }
    /* Preparing string for config path in home dir. */
    /* Подготовка строки с путем конфиг.файла в домашней директории. */
    _path_home_config = "/home/";
    _path_home_config += passwd->pw_name;
    _path_home_config += "/";
    _path_home_config += ".kiosk.conf";
    /* Also preparing string for log file. */
    /* Подготовка строки с путем лог.файла в домашней директории. */
    _path_home_log = "/home/";
    _path_home_log += passwd->pw_name;
    _path_home_log += "/";
    _path_home_log += ".kioskd.log";
    /* Done */
    /* Готово */
    return (0);
}

int Kioskd::_prepare_config_and_log_streams(void) {

    /* Trying to open config and log files. */
    /* Попытка открыть файлы конфигурации и логирования. */
    _config.open(_path_home_config);
    if (!_config.good()) {
        std::cerr << "ERROR: Can't open config file\n";
        return (1);
    }
    _log.open(_path_home_log, std::ios::app);
    if (!_log.good()) {
        std::cerr << "ERROR: Can't open log file\n";
        return (1);
    }
    return (0);
}

void Kioskd::_put_log(char const *str) {

    /* This one just put message to the log. */
    /* Функция логирования с подставлением времени. */
    time_t rawtime;
    struct tm *info;
    char buffer[80];
    time(&rawtime);
    info = localtime(&rawtime);
    strftime(buffer, 80, "%x - %I:%M%p", info);
    _log << "| " << buffer << " | " << str << '\n';
}

void Kioskd::_close_xsession(void) {

    /* Function close a x-session. */
    /* Функция завершает х-сессию. */
    _put_log("INFO: Clossing xsession");
    pid_t logout = fork();
    if (!logout) {
        int user_id = getuid();
        execl("/usr/bin/pkill", "pkill", "-kill", "-u", std::to_string(user_id).data(), NULL);
        _put_log("ERROR: Logout crash");
        exit(EXIT_FAILURE);
    }
}

void Kioskd::run_all_paths_from_config(void) {

    /* Preparing variables. */
    /* Подготовка переменных. */
    std::string tmp;
    std::vector<char *> vector_of_arguments;
    std::string delimiter = " ";
    std::string token;
    /* Reading and starting applications line by line from a configuration file. */
    /* Построчное считываение и запуск приложений из файла конфигурации. */
    while (getline(_config, tmp)) {
        if (tmp.length() == 0) {
            _put_log("INFO: Empty line");
            continue;
        }
        /* Preparing variables. */
        /* Подготовка переменных. */
        char **array_of_arguments = NULL;
        size_t pos = 0;
        vector_of_arguments.clear();
        /* Splitting a string into arguments before running. */
        /* Разделение строки на аргументы перед запуском. */
        while ((pos = tmp.find(delimiter)) != std::string::npos) {
            token = tmp.substr(0, pos);
            vector_of_arguments.push_back(strdup(token.data()));
            tmp.erase(0, pos + delimiter.length());
        }
        vector_of_arguments.push_back(strdup(tmp.data()));
        /* Last Pointer must be NULL. */
        /* Последний поинтер должен быть 0. */
        vector_of_arguments.push_back(nullptr);
        /* Getting pointer to the first element before starting. */
        /* Получение поинтера на первый элемент перед запуском. */
        array_of_arguments = vector_of_arguments.data();
        /* Starting ONE application from ONE line in config file. */
        /* Запуск ОДНОГО приложения ОДНОЙ строки конфигурационного файла. */
        pid_t cur_app_from_config = fork();
        if (!cur_app_from_config) {
            execv(array_of_arguments[0], array_of_arguments);
            _put_log("ERROR: Error execv()");
            exit(EXIT_FAILURE);
        }
        /* Putting log. */
        /* Запись событий в лог. */
        std::stringstream log_entry;
        log_entry << "INFO: Run line from config: " << tmp << " PID: " << cur_app_from_config;
        _put_log(log_entry.str().data());
        
        for (auto cur : vector_of_arguments) {
            free(cur);
        }
        /* Grab PID for Kiosk. */
        /* Сбор пида для дальнейшего отслеживания в Киоске. */
        _pids_for_kiosk.push_back(cur_app_from_config);
    }
    /* For case "empty config". */
    /* Фиксация состояния пустого конфига. */
    if (vector_of_arguments.empty()) {
        _put_log("ERROR: Empty config!");
    }
}

int Kioskd::main_cycle(void) {

    /* Preparing variables. */
    /* Подготовка переменных. */
    char **array_of_arguments = NULL;
    std::vector<char *> vector_of_arguments;
    /* Array preparation before starting a kiosk with all pids. */
    /* Подготовка массия со всеми пидами перед запуском киоска. */
    vector_of_arguments.push_back(strdup("/usr/sbin/kiosk"));
    for (auto cur : _pids_for_kiosk) {
        vector_of_arguments.push_back(strdup(std::to_string(cur).data()));
    }
    vector_of_arguments.push_back(nullptr);
    array_of_arguments = vector_of_arguments.data();
    /* Putting log. */
    /* Запись событий в лог. */
    _put_log("INFO: Main cycle run");
    /* Starting main cycle. */
    /* Запуск основного цикла. */
    int flag = 1;
    pid_t kiosk = 0;
    while (true) {
        /* Flag helps restart kiosk in case of x-server failure. */
        /* Флаг помогает перезапускать киоск. */
        if (flag == 1) {
            kiosk = fork();
            if (!kiosk) {
                execv(array_of_arguments[0], array_of_arguments);
                _put_log("ERROR: Error execv()");
                exit(EXIT_FAILURE);
            }
        }
        /* Waiting for completion and analysis of the result. */
        /* Ожидание завершения любого процесса и анализ. */
        int status;
        int res = waitpid(0, &status, 0);
        /* If the process ends from the config file, then delete it from the observation. */
        /* Если завершенный процесс из конфиг.файла, то наблюдение за этим пидом прекращается. */
        for (auto iterator = _pids_for_kiosk.begin(); iterator != _pids_for_kiosk.end(); ++iterator) {
            if (res == *iterator) {
                std::stringstream log_entry;
                log_entry << "INFO: PID " << res << " is stoped";
                _put_log(log_entry.str().data());
                _pids_for_kiosk.erase(iterator);
                iterator = _pids_for_kiosk.begin();
            }
            if (_pids_for_kiosk.empty()) {
               break;
            }
        }
        /* In the absence of observed pids, the daemon ends the x-session. */
        /* Если наблюдаемые пиды закончились, тогда демон завершает х-сессию. */
        if (_pids_for_kiosk.empty()) {
            _put_log("INFO: Vector of PIDs empty. Work done.");
            break;
        }
        /* If the kiosk is completed, then it will be restarted. */
        /* Если завершился сам киоск, то он будет перезапущен. */
        if (res == kiosk) {
            flag = 1;
            _put_log("WARNING: Kiosk reloading");
        } else {
            flag = 0;
        }
    }
    /* Мemory clearing. */
    /* Освобождение ресурсов. */
    for (auto cur : vector_of_arguments) {
        free(cur);
    }
    /* Ending x-session. */
    /* Завершение х-сессии. */
    _close_xsession();
    return (0);
}

Kioskd::Kioskd(void) {

    /* The constructor prepares streams and can exit if there is an error. */
    /* Подготовка файлов, завершение работы в случае неудачи. */
    if (_prepare_config_and_log_strings()) {
        _close_xsession();
        exit(EXIT_FAILURE);
    }
    if (_prepare_config_and_log_streams()) {
        _close_xsession();
        exit(EXIT_FAILURE);
    }
    _put_log("INFO: Kiosk start successful");
}

Kioskd::~Kioskd(void) {

    /* Log and closing streams. */
    /* Журналирование о завершении работы и закрытие файлов. */
    _put_log("INFO: Kiosk stoping!");
    _log.close();
    _config.close();
}