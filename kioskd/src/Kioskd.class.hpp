/* *************************************************************************************************** */
/*                                                                                                     */
/*                                                          :::::::::   ::::::::   ::::::::      :::   */
/*  Kioskd.class.hpp                                       :+:    :+: :+:    :+: :+:    :+:   :+: :+:  */
/*                                                        +:+    +:+ +:+    +:+ +:+         +:+   +:+  */
/*  By: Ivan Marochkin <i.marochkin@rosalinux.ru>        +#++:++#:  +#+    +:+ +#++:++#++ +#++:++#++:  */
/*                                                      +#+    +#+ +#+    +#+        +#+ +#+     +#+   */
/*  Created: 2020/01/22 11:28:47 by Ivan Marochkin     #+#    #+# #+#    #+# #+#    #+# #+#     #+#    */
/*  Updated: 2020/01/22 11:28:47 by Ivan Marochkin    ###    ###  ########   ########  ###     ###     */
/*                                                                                                     */
/* *************************************************************************************************** */

#ifndef __KIOSKD_CLASS_HPP__
#define __KIOSKD_CLASS_HPP__

#define DEF_BUF 128

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <array>

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/keysymdef.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <pwd.h>

/*
    The class writes a log file and reads the configuration from the file.
    In case of failure to open files, the x-session ends. It also launches applications
    from the config file, launches a kiosk and monitors the status of these applications and the kiosk.
    
    Класс пишет лог-файл и читает конфигурацию из файла.
    В случае сбоя открытия файлов х-сессия завершается. Так же производит запуск приложений
    из файла конфигурации, запуск киоска и отслеживает состояния этих приложений.
*/

class Kioskd {

private:
    std::string _path_home_config;
    std::string _path_home_log;
    std::ofstream _log;
    std::ifstream _config;
    std::vector<pid_t> _pids_for_kiosk;
    int _prepare_config_and_log_strings(void);
    int _prepare_config_and_log_streams(void);
    void _put_log(char const *str);
    void _close_xsession(void);

public:
    Kioskd(void);
    ~Kioskd(void);
    void run_all_paths_from_config(void);
    int main_cycle(void);
};

#endif /* __KIOSKD_CLASS_HPP__ */