add_executable(kioskd Kioskd.class.cpp main.cpp)
target_link_libraries(kioskd ${X11_LIBRARIES})
install(TARGETS kioskd DESTINATION ${CMAKE_INSTALL_SBINDIR})
