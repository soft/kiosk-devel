#!/bin/bash

# ***************************************************************************************************** #
#                                                                                                       #
#                                                           :::::::::   ::::::::   ::::::::      :::    #
#   install.sh                                             :+:    :+: :+:    :+: :+:    :+:   :+: :+:   #
#                                                         +:+    +:+ +:+    +:+ +:+         +:+   +:+   #
#   By: Ivan Marochkin <i.marochkin@rosalinux.ru>        +#++:++#:  +#+    +:+ +#++:++#++ +#++:++#++:   #
#                                                       +#+    +#+ +#+    +#+        +#+ +#+     +#+    #
#   Created: 2020/01/20 17:37:54 by Ivan Marochkin     #+#    #+# #+#    #+# #+#    #+# #+#     #+#     #
#   Updated: 2020/01/20 17:37:54 by Ivan Marochkin    ###    ###  ########   ########  ###     ###      #
#                                                                                                       #
# ***************************************************************************************************** #

set -o xtrace
shopt -s extglob

C_CYAN="\033[35;1m"
C_RED="\033[31;1m"
C_GREEN="\033[32;1m"
C_RESET="\033[0m"

if [ $# -eq 0 ]
then
        echo -e "${C_RED}Usage ./install [user_name]${C_RESET}"
        exit 1
fi

INPUT=$1
USER_HOME="/home/${INPUT}"
if [ -d $USER_HOME ]
then
        echo -e "${C_GREEN}User found${C_RESET}"
else
        echo -e "${C_RED}User not found${C_RESET}"
        exit 1
fi

echo -e "${C_CYAN}Try to install KIOSKD to /usr/sbin${C_RESET}"
cp ./kioskd/kioskd /usr/sbin
if [ $? -eq 0 ]
then
	echo -e "${C_GREEN}Done${C_RESET}"
else
	echo -e "${C_RED}Stop. Try sudo ./install.sh or KIOSKD install already${C_RESET}"
	exit 1
fi

echo -e "${C_CYAN}Try to install KIOSK to /usr/sbin${C_RESET}"
cp ./kiosk/kiosk /usr/sbin
if [ $? -eq 0 ]
then
	echo -e "${C_GREEN}Done${C_RESET}"
else
	echo -e "${C_RED}Stop. Try sudo ./install.sh or KIOSK install already${C_RESET}"
	exit 1
fi

USER_HOME_CONF_DIR_AUTOSTART="${USER_HOME}/.config/autostart"
echo -e "${C_CYAN}Checking autostart dir${C_RESET}"
if [ -d $USER_HOME_CONF_DIR_AUTOSTART ]
then
        echo -e "${C_GREEN}Autostart dir found${C_RESET}"
else
        echo -e "${C_CYAN}Create autostart dir${C_RESET}"
        mkdir $USER_HOME_CONF_DIR_AUTOSTART
fi

USER_HOME_CONF_DIR_AUTOSTART_LINK="${USER_HOME_CONF_DIR_AUTOSTART}/kiosk.desktop"
echo -e "${C_CYAN}Checking autostart link${C_RESET}"
if [ -e $USER_HOME_CONF_DIR_AUTOSTART_LINK ]
then
        echo -e "${C_RED}Autostart link found. Delete or move ${USER_HOME_CONF_DIR_AUTOSTART_LINK}${C_RESET}"
        exit 1
else
        echo -e "${C_GREEN}Autostart link not found${C_RESET}"
fi

echo -e "${C_CYAN}Create autostart link${C_RESET}"
cp ./to_install/kioskd.desktop $USER_HOME_CONF_DIR_AUTOSTART
if [ $? -eq 0 ]
then
	echo -e "${C_GREEN}Done${C_RESET}"
else
	echo -e "${C_RED}Create autostart link fault${C_RESET}"
	exit 1
fi

chown -R root:root $USER_HOME_CONF_DIR_AUTOSTART

echo -e "${C_CYAN}Prepair other files${C_RESET}"
touch "${USER_HOME}/.kiosk.conf"
touch "${USER_HOME}/.kioskd.log"
touch "${USER_HOME}/.kiosk.log"
chmod 666 "${USER_HOME}/.kioskd.log"
chmod 666 "${USER_HOME}/.kiosk.log"

echo -e "${C_GREEN}Install successful!${C_RESET}"