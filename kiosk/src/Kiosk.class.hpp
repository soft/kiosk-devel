/* *************************************************************************************************** */
/*                                                                                                     */
/*                                                          :::::::::   ::::::::   ::::::::      :::   */
/*  Kiosk.class.hpp                                        :+:    :+: :+:    :+: :+:    :+:   :+: :+:  */
/*                                                        +:+    +:+ +:+    +:+ +:+         +:+   +:+  */
/*  By: Ivan Marochkin <i.marochkin@rosalinux.ru>        +#++:++#:  +#+    +:+ +#++:++#++ +#++:++#++:  */
/*                                                      +#+    +#+ +#+    +#+        +#+ +#+     +#+   */
/*  Created: 2020/01/22 11:28:47 by Ivan Marochkin     #+#    #+# #+#    #+# #+#    #+# #+#     #+#    */
/*  Updated: 2020/01/22 11:28:47 by Ivan Marochkin    ###    ###  ########   ########  ###     ###     */
/*                                                                                                     */
/* *************************************************************************************************** */

#ifndef __KIOSK_CLASS_HPP__
#define __KIOSK_CLASS_HPP__

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <array>

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/keysymdef.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <pwd.h>

class Kiosk {

private:
    char **_av;
    Display *_display;
    Atom _atomPID;
    std::vector<pid_t> _skiping_pids;
    std::vector<Window> _root_windows_of_skiping_pids;
    std::ofstream _log;
    int _prepare_log(void);
    void _find_childs_of_skiping_pids(void);
    void _find_root_windows_of_skiping_pids(void);
    int _find_root_windows_of_skiping_pids_recursive(Window current_window);
    void _get_atom_pid_request(void);
    void _unmap_all_except_root_windows_of_skiping_pids(Window current_window);
    void _put_log(char const *str);

public:
    Kiosk(char **av);
    ~Kiosk(void);
    void main_cycle(void);
};

#endif /* __KIOSK_CLASS_HPP__ */