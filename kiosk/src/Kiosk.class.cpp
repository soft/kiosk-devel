/* *************************************************************************************************** */
/*                                                                                                     */
/*                                                          :::::::::   ::::::::   ::::::::      :::   */
/*  Kiosk.class.cpp                                        :+:    :+: :+:    :+: :+:    :+:   :+: :+:  */
/*                                                        +:+    +:+ +:+    +:+ +:+         +:+   +:+  */
/*  By: Ivan Marochkin <i.marochkin@rosalinux.ru>        +#++:++#:  +#+    +:+ +#++:++#++ +#++:++#++:  */
/*                                                      +#+    +#+ +#+    +#+        +#+ +#+     +#+   */
/*  Created: 2020/01/28 13:07:35 by Ivan Marochkin     #+#    #+# #+#    #+# #+#    #+# #+#     #+#    */
/*  Updated: 2020/01/28 15:34:22 by Ivan Marochkin    ###    ###  ########   ########  ###     ###     */
/*                                                                                                     */
/* *************************************************************************************************** */

#include "Kiosk.class.hpp"

int Kiosk::_prepare_log(void) {

    /* Prepares paths before opening files. */
    /* Подготовливает пути перед открытием файлов. */
    std::string path_home_log;
    struct passwd *passwd;
    int user_id = getuid();
    if (!user_id) {
        std::cerr << "ERROR: Can't detect user id.\n";
        return (1);
    }
    /* Request to PASSWD for user information. */
    /* Запрос информации о пользователе. */
    passwd = getpwuid(user_id);
    if (!passwd) {
        std::cerr << "ERROR: Can't detect user name from getpwuid.\n";
        return (1);
    }
    /* Also preparing string for log file. */
    /* Подготовка строки с путем лог.файла в домашней директории. */
    path_home_log = "/home/";
    path_home_log += passwd->pw_name;
    path_home_log += "/";
    path_home_log += ".kiosk.log";
    /* Trying to open log files. */
    /* Попытка открыть файл логирования. */
    _log.open(path_home_log, std::ios::app);
    if (!_log.good()) {
        return (1);
    }
    return (0);
}

void Kiosk::_find_childs_of_skiping_pids(void) {

    /* Grab all arguments pids into vector. */
    /* Сбор аргументов с пидами в вектор. */
    _skiping_pids.clear();
    for (int i = 1; _av[i]; ++i) {
        _skiping_pids.push_back(atoi(_av[i]));
    }
    /* Check all pids for presence childs. */
    /* Проверка наличия потомков у запущенных процессов. */
    for (pid_t cur : _skiping_pids) {
        std::string prog = "pgrep -P " + std::to_string(cur);
        char tt[128];
        memset(tt, 0, 128);
        FILE *df = popen(prog.data(), "r");
        while (fscanf(df, "%s", (char *)&tt) > 0) {
            _skiping_pids.push_back(atoi(tt));
            memset(tt, 0, 128);
        }
        pclose(df);
    }
}

void Kiosk::_find_root_windows_of_skiping_pids(void) {

    /* Preparing and filling in variables. */
    /* Подготовка и наполнение переменных. */
    _root_windows_of_skiping_pids.clear();
    unsigned children_count;
    Window current, root, parent, *children;
    current = XDefaultRootWindow(_display);
    int res = XQueryTree(_display, current, &root, &parent, &children, &children_count);
    if (!res) {
        _put_log("crash in find_run_it_parents");
    }
    /* Run recursive search on tree branches. */
    /* Запуск рекурсивного поиска по ветка дерева. */
    for (unsigned i = 0; i < children_count; ++i) {
        int res = _find_root_windows_of_skiping_pids_recursive(children[i]);
        if (res) {
            _root_windows_of_skiping_pids.push_back(children[i]);
        }
    }
}

int Kiosk::_find_root_windows_of_skiping_pids_recursive(Window current_window) {
    
    /* Preparing variables. */
    /* Подготовка переменных. */
    unsigned children_count;
    Window root, parent, *children;
    Atom type;
    int format;
    unsigned long nItems, bytesAfter;
    unsigned char *propPID = 0;
    char *name = NULL;
    /* Skip lock screen window. */
    /* Не блокировать окна скрин-лока. */
    if (XFetchName(_display, current_window, &name) > 0) {
        if (strcmp(name, "kscreenlocker_greet") == 0) {
            return (1);
        }
        XFree(name);
    }
    /* PID window request. */
    /* Запрос пида у текущего окна. */
    if (Success == XGetWindowProperty(_display, current_window, _atomPID, 0, 1, False, XA_CARDINAL, &type, &format, &nItems, &bytesAfter, &propPID)) {
        if (propPID != 0) {
            for (pid_t cur : _skiping_pids) {
                if (*((unsigned long *)propPID) == (unsigned)cur) {
                    return (1);
                }
            }
            XFree(propPID);
        }
    } else {
        _put_log("Bad try get window pid");
    }
    /* Request for tree of current window. */
    /* Запрос дерева для текущего окна. */
    int res = XQueryTree(_display, current_window, &root, &parent, &children, &children_count);
    if (!res) {
        _put_log("Bad try XQueryTree in find_recursive");
    }
    /* Run tree search for current window. */
    /* Запуск перебора дерева текущего окна. */
    for (unsigned i = 0; i < children_count; ++i) {
        int check = (_find_root_windows_of_skiping_pids_recursive(children[i]));
        if (check == 1) {
            return (1);
        }
    }
    /* Мemory clearing. */
    /* Освобождение ресурсов. */
    XFree(children);
    return (0);
}

void Kiosk::_get_atom_pid_request(void) {

    /* Preparing a request in the x-server API. */
    /* Подготовка запроса в API х-сервера. */
    _atomPID = XInternAtom(_display, "_NET_WM_PID", True);
    if (_atomPID == None) {
        _put_log("Bad try atom");
    }
}

void Kiosk::_unmap_all_except_root_windows_of_skiping_pids(Window current_window) {

    /* Preparing and filling in variables. */
    /* Подготовка и наполнение переменных. */
    Window root, parent, *children;
    unsigned children_count;
    int res = XQueryTree(_display, current_window, &root, &parent, &children, &children_count);
    if (!res) {
        _put_log("Bad try XQueryTree in unmap_recursive");
    }
    /* Recursive search and hiding tree branches. */
    /* Рекурсивный перебор и скрытие веток дерева */
    for (unsigned i = 0; i < children_count; ++i) {
        int flag = 0;
        for (Window tmp : _root_windows_of_skiping_pids) {
            if (children[i] == tmp) {
                ++flag;
            }
        }
        if (!flag) {
            _unmap_all_except_root_windows_of_skiping_pids(children[i]);
            XUnmapWindow(_display, children[i]);
        }
    }
    /* Мemory clearing. */
    /* Освобождение ресурсов. */
    XFree(children);
}

void Kiosk::_put_log(char const *str) {

    /* This one just put message to the log. */
    /* Функция логирования с подставлением времени. */
    time_t rawtime;
    struct tm *info;
    char buffer[80];
    time(&rawtime);
    info = localtime(&rawtime);
    strftime(buffer, 80, "%x - %I:%M%p", info);
    _log << "| " << buffer << " | " << str << '\n';
}

Kiosk::Kiosk(char **av) {

    /* Constructor prepares a log, connects to the display and prepares a request for pid. */
    /* Конструктор подготавливает лог, подключается к дисплею и готовит запрос на пид. */
    if (_prepare_log()) {
        std::cerr << "Can't open log file";
    }
    if ((_display = XOpenDisplay(getenv("DISPLAY"))) == NULL) {
        _put_log("XOpenDisplay error");
        exit(EXIT_FAILURE);
    }
    _av = av;
    _get_atom_pid_request();
}

Kiosk::~Kiosk(void) {

    /* Ending work. */
    /* Завершение работы. */
    XCloseDisplay(_display);
    _log.close();
}

void Kiosk::main_cycle(void) {

    /* Preparing and filling in variables. */
    /* Подготовка и наполнение переменных. */
    Window parent, *children;
    unsigned children_count;
    XEvent e;
    unsigned long event_mask = SubstructureNotifyMask;
    Window root = XDefaultRootWindow(_display);
    XSelectInput(_display, root, event_mask);
    /* The first iteration of the main cycle, regardless of the x-server events. */
    /* Первая итерация основного цикла вне зависимости от событий х-сервера. */
    _find_childs_of_skiping_pids();
    _find_root_windows_of_skiping_pids();
    _unmap_all_except_root_windows_of_skiping_pids(root);
    /* The main cycle with the response to the x-server event. */
    /* Основной цикл с реакцией на событие х-сервера. */
    while (1) {
        XNextEvent(_display, &e);
        if (e.type == 19) {
	        usleep(10);
            _find_childs_of_skiping_pids();
            _find_root_windows_of_skiping_pids();
            int res = XQueryTree(_display, XDefaultRootWindow(_display), &root, &parent, &children, &children_count);
            _root_windows_of_skiping_pids.push_back(children[children_count - 1]);
            _unmap_all_except_root_windows_of_skiping_pids(root);
            if (!res) {
                _put_log("Bad request XQueryTree in main_cycle");
            } else {
                XFree(children);
            }
        }
    }
}