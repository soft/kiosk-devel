/* *************************************************************************************************** */
/*                                                                                                     */
/*                                                          :::::::::   ::::::::   ::::::::      :::   */
/*  main.cpp                                               :+:    :+: :+:    :+: :+:    :+:   :+: :+:  */
/*                                                        +:+    +:+ +:+    +:+ +:+         +:+   +:+  */
/*  By: Ivan Marochkin <i.marochkin@rosalinux.ru>        +#++:++#:  +#+    +:+ +#++:++#++ +#++:++#++:  */
/*                                                      +#+    +#+ +#+    +#+        +#+ +#+     +#+   */
/*  Created: 2019/12/25 16:08:10 by Ivan Marochkin     #+#    #+# #+#    #+# #+#    #+# #+#     #+#    */
/*  Updated: 2019/12/25 16:08:10 by Ivan Marochkin    ###    ###  ########   ########  ###     ###     */
/*                                                                                                     */
/* *************************************************************************************************** */

#include "main.hpp"

/*
    KIOSK ROSA (c) "NTC IT ROSA" 2020

    The application receives pids as arguments, goes through the x-server window tree and hides
    all branches except those containing pids.

    Приложение получает на вход в качестве аргументов пиды, проходит по дереву окон х-сервера и
    скрывает все ветки, кроме содержащих пиды.
*/

int ApplicationErrorHandler(Display *display, XErrorEvent *event) {
    
    display = display;
    event = event;
    return (1);
}

int ApplicationIOErrorHandler(Display *display) {

    display = display;
    return (1);
}

int main(int ac, char **av) {

    if (ac < 2) {
        std::cout << "usage: ./kiosk [PID1] [PID2] ...\n";
        exit(EXIT_SUCCESS);
    }
    /* Set custom error handler for x-server. */
    /* Установка собственных обработчиков ошибок */
    XSetErrorHandler(ApplicationErrorHandler);
    XSetIOErrorHandler(ApplicationIOErrorHandler);
    /* Creating kiosk object and start main cycle */
    /* Создание киоска и запуск основного цикла */
    Kiosk kiok(av);
    kiok.main_cycle();
    /* gg */
    return (0);
}
